# EMPIRE
## Class 1 battlegrounds

Empire-class maps are used in premiere 1 vs 1 tournaments. They are balanced around the ongoing meta, with enough testing to confirm that they provide a canvas for great matches!

This document logs changes to Empire-class maps. The current map listing always resides below this notice, followed by changes to the individual maps. Changes are listed from newest to oldest.

## Empire-class maps - 26 February 2025
- (2)Apollo 1.3 by Btyb
- (2)Demilune 1.4 by JujiK
- (2)Dyadic Shore 3.0 by Btyb
- (2)Frosted Forest 1.1 by Btyb
- (2)Genesis Y 1.5 by Btyb
- (2)Germination 3.0 by Btyb
- (2)Near Impact 1.1 by JujiK
- (2)Serenity 1.1 by Btyb & JujiK
- (4)Blackwater Bastion 1.3 by Btyb
- (4)Checkstealer 1.1 by Pr0nogo
- (4)Paradise 1.3 by Btyb
- (4)Tesseract 1.0 by Pr0nogo

## Map pool changes -- 26 February 2025
- OUT TO CLASS 0: Potidaea, Decapitation
- OUT TO CLASS 2: Cold Sweat, Muspelheim, Old Scars, Spinaltap, Rageslave
- IN FROM CLASS 0: Apollo, Dyadic Shore 3, Frosted Forest, Germination 3, Radium, Serenity, Tesseract

## (2)Potidaea 1.2 -- 16 January 2025
- Made bridges into natural (mostly) unbuildable
	- Will need feedback as to whether this is helpful/harmful
- Blocked pathing in unwanted drop zones
- Removed neutral town centers
- Added Biotic Bastions to block island town center placement

## (2)Demilune 1.4 -- 6 January 2025
- Attempted to remove pathfinding regions that had no exit pathing

## (2)Demilune 1.3 -- 16 December 2024
- Add missing Start Location for final Witness slot

## (4)Decapitation 1.2 -- 22 November 2024
- Added Map Revealers to show Monuments of Sin
- Updated decor near the northern ramp complex

## (2)Genesis Y 1.5 -- 21 November 2024
- Made various tile fixes

## (2)Demilune 1.2 -- 21 November 2024
- Made various tile fixes

## (2)Potidaea 1.1 -- 18 November 2024
- Made various tile fixes
- Updated map description

## (2)Near Impact 1.1 -- 25 October 2024
- Added near-depleted Mineral Field nodes for natural wall assists

## (2)Demilune 1.1 -- 25 October 2024
- Removed smaller third ramp from large high ground pods, so that army movements against the 12/6 backstab route require more deliberate pathing
- Removed secondary ramp from 3/9 to offer incentive to expand and hold the position across your bridge as a potential 3rd
- Reduced 6/12 and 3/9 buildability around ramps to make them slightly easier to assault

## (3)Rageslave 1.2 -- 25 October 2024
- Reworked middle to be low-ground asteroid
- Adjusted spacing around some low-ground expansions

## (2)Muspelheim 1.4 -- 24 October 2024
- Sealed several drop harassment spots from 3/9 o'clock to naturals (r. Art_Of_Turtle)
- Replaced central shale terrain with ash
- Improved visuals throughout the map

## (4)Decapitation 1.1 -- 23 October 2024
- Added two neutral Monuments of Sin to the four central pathways into the center
- Give crushed rock on highground a thematic facelift

## (2)Spinaltap 1.3 -- 23 October 2024
- Make Mineral-only bases slightly more spacious
- Correct minor asymmetry near Mineral-only bases
- Slightly increase buildspace in 3/9 o'clocks
- Crunch siege spots in upper and lower middle ramp areas

## Global change -- 19 October 2024
- Default Mineral Field amounts 1500 » 2000

## Map pool changes -- 19 October 2024
- OUT: Impetus, Oblivion, Maxiom
- IN: Demilune, Near Impact, Potidaea, Decapitation

## (2)Spinaltap 1.2 -- 19 October 2024
- Natural wall-assist Mineral Field amounts 8 » 15; now requires 2 trips to deplete

## (4)Checkstealer 1.1 -- 19 October 2024
- Natural expansion Mineral Field counts 6 » 7
- Cardinal base Vespene Ridge counts 0 » 2
- Converted center base into island

## (4)Maxiom 1.2 -- 2 October 2024
- Sealed unintended drop holes throughout the map
- Made minor decor adjustments

## (2)Impetus 2.3 -- 2 October 2024
- Sealed unintended drop holes throughout the map
- Made minor decor adjustments

## (2)Spinaltap 1.1 -- 1 October 2024
- Added near-empty Mineral Field nodes in natural chokes to help walls
- Widened bridges from naturals into map center
- Widened chokes into 3/9 o'clock and 4/10 o'clock

## Map pool changes -- 28 September 2024
- OUT: Derelict, Radium
- IN: Spinaltap, Checkstealer

## (4)Blackwater Bastion 1.3 -- 28 September 2024
- Markedly improve resource layouts in all bases
- Third base Mineral Field count 8 » 7
- Cardinal base Mineral Field count 7 » 8
- Fix various tile issues

## (2)Muspelheim 1.3 - 25 September 2024
- Added extra base at 11 o'clock and 5 o'clock
- Adjusted resource counts

## (4)Paradise 1.3 -- 17 September 2024
- Finally remove the Btyb tile (and correct other tiles)

## Global change -- 16 September 2024
- Updated resources on all maps for new economy paradigm

## (2)Muspelheim 1.1 -- 13 September 2024
- Added Map Revealers to reveal Iron Foundries

## (2)Genesis Y 1.3 -- 18 July 2024
- Adjusted Geyser placement for consistent resource return rate

## (2)Oblivion 1.3 -- 15 July 2024
- Fixed terrain hole in bottom-right main

## (2)Oblivion 1.2 -- 13 July 2024
- Fixed stuck spot in center (r. Krankendau)

## (2)Genesis Y 1.2 -- 30 June 2024
- Fixed ramp vortexes (r. Krankendau)
- Improved pathability across the map
- Corrected symmetry issues

## (4)Blackwater Basation 1.1 -- 30 June 2024
- Added doodads for visual flair

## (2)Genesis Y 1.1 -- 26 June 2024
- Main base Mineral Field count 9 » 8
- Improved resource gather rates across the map

## (2)Oblivion 1.1 -- 29 May 2024
- Nudged main resources away from negative space
- Shifted town center placement tiles in low ground bases
- Slightly opened up terrain near upper and lower center ramps

## (4)Paradise 1.1 -- 28 May 2024
- Removed cliff adjacent to naturals
- Added slight cover to thirds

## (2)Derelict 2.0 -- 8 April 2024
- All neutral town centers now have 40% health
- Slightly extended main towards 12/6 o'clock
- Retracted natural overlook, slightly tightened natural choke
- Thirds:
    - Moved slightly towards mains, made smaller overall
    - Tightened choke towards 12/6 o'clock
    - Added small overhang from main to 3rd
- 12/6 o'clock: changed two small ramps into one large ramp
- 9/3 o'clock: added small terrain block at top of ramp
- Corner expos:
    - Now lowest ground, island
    - Has neutral town center
- Added 11.5/5.5 expos, with neutral anchor for early-game
- Middle area reworked
    - Now open low ground in center
    - Ministry instead of Treasury
    - Geysers on nearby compound: can be used by T1 workers, but more efficient with T2 workers
- Added 4 extra compound towers in general middle of map for cover and drops

## (2)Germination 1.1 -- 28 March 2024
- Fixed south spawn being able to harvest the flyer expo resources from the low ground
- Fixed a few height issues througout the map

## (2)Titan Forge 1.2 -- 21 March 2024
- Expanded map eastwards by 16 tiles
- Corrected various asymmetries and resarea assignments
- Tided resource layouts throughout the map to improve harvesting
- Merged ramps into center-left low ground
- Significantly opened up pathability of central pathway
- Added more ramps into central bases, and slightly improved pathability
- Added 8 Mineral Fields to central bases, and removed 1 Geyser (now 2 total)
- Added unit-only highrise for defending central attacks into 2 / 4 o'clock bases

## (2)Fata Morgana 1.2 -- 21 March 2024
- Expanded map southwards by 16 tiles
- Added more pathability to center, facing 3 / 9 o'clock ramps
- Expanded all north-facing central ramps by 2 tiles
- Fused south-facing central ramps
- Added cover to help split armies in southern pathway
- Moved 6 o'clock base to new south border
- Shifted 5 and 7 o'clock bases closer to compound

## (2)Impetus 2.1 -- 17 March 2024
- Added a third entrance to high ground side bases
- Added Vespene Ridge to thirds and corner bases
- Refined terrain in mid and on approaches to high ground side bases
- Set in-game name to "Impetus 2"

## (2)Impetus 2.0 -- 11 March 2024
- Significantly reworked main & natural layout
- Significantly reworked the center of the map
- Significantly reworked the resource layouts
- Adjusted negative space in few places

## (2)Axiom 1.1 -- 6 March 2024
- Expanded ramps from mains into naturals
- Expanded all ramps into low platform areas
- Adjusted terrain around neutral Treasuries
- Tucked in 12/6 o'clock expansions for better ground movement
- Made adjustments to resource layouts throughout the map
- Sealed many drop holes throughout the map
- Cleaned up visuals throughout the map

## (2)Sideshow 1.2 -- 6 March 2024
- Added 1 Mineral Field to top right / bottom left expansions (6 total)
- Expanded all single ramps into double
- Further expanded ramp from thirds into 12/6 o'clock expansions
- Cleaned up pattern of unbuildable terrain outside 3/9 o'clock expansions

## (2)Derelict 1.2 -- 5 March 2024
- Significantly expanded ramps into 3/9 o'clock expansions
- Added 1 Geyser and 2 Mineral Fields to 3/9 o'clock expansions
- Widened small ramps into central platforms

## (4)Highwater 1.2 -- 5 March 2024
- Widened new ramps out of mains
- Significantly expanded ramps into central north/south courtyards
- Slightly increased negative space in thirds
- Added some cover to courtyards
- Added a touch of evil to the decor

## (2)Derelict 1.1 -- 4 March 2024
- Removed Ridges in mains
- Mains: slightly reduced buildspace in defensive overlook
- Added Ridges to 12/6 o'clock expansions
- Fused defender-side ramps in middle for more open terrain
- Removed cover cliff by 10/4 o'clock expansions

## (2)Sideshow 1.1 -- 4 March 2024
- Removed Ridges in mains
- Shifted Geysers closer to town centers in mains
- Added one Mineral Field to thirds (7 total)
- Added thin bridges acting as shortcuts into middle
- Slightly reduced buildable space near new bridges

## (4)Highwater 1.2 -- 4 March 2024
- Added small ramp from mains towards thirds
- Added path joining thirds to each other
- Added path joining thirds to central north/south courtyards
- Added 1 Mineral Field to thirds (7 total)
- Added 2 Mineral Fields to fourths (6 total)

## (2)Impetus 1.1 -- 20 February 2024
- Shifted mains a few tiles away from ramps
- Shifted naturals a few tiles closer to ramps/chokes
- Added more negative space behind naturals
- Thirds: moved inward ramps closer to naturals
- Thirds: slightly reduced buildspace near outward ramps
- Sealed small back path into 12/6 o'clock expansions
- Added a bit more negative space to 10/4 o'clock expansions