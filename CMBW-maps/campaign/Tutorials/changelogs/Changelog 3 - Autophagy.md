## 14 May 2024
- **Triggers:**
	- Fixed wrong sound file for Ragant in "lose many units" dialogue event (r. lolrsk8s)
- **Layout:**
	- Added new path from bottom right double-expansion to center
	- Adjusted decor across the entire map
- **AI:**
	- Fixed anti-air defense requests not processing properly

## 13 May 2024
- **Initial prerelease!**