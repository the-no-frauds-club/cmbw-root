## 13 May 2024
- **Triggers:**
	- In victory, mission objective is now marked as complete

## 2 May 2024
- **Triggers:**
	- Ported to new trigger parser
	- Enabled alternate intro via bank data

## 30 March 2024
- **Initial prerelease!**