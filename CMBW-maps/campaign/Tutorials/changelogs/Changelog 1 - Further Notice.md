## 2 May 2024
- **General:**
	- Ported to new trigger parser
	- Enabled alternate intro via bank data
	- Prevented "found expansion" event while inside Osina's base

## 20 February 2024
- **Starting conditions:**
	- Adjusted position of tertiary Vespene Ridge in Osina main
- **AI:**
	- Fixed Vespene Ridge resource area binding in Osina natural
	- Improved Osina's base layouts in main & natural
	- Adjusted Osina's build order to take into account the latest techtree changes
- **Audiovisuals:**
	- Added a 2-minute delay between opening combat events
	- Made very minor cliff variant changes

## 19 February 2024
- **Starting conditions:**
	- Significantly toned down initial fog of war reveal to better encourage scouting
	- Shuffled Mineral Field layout in Cettocci and Osina mains for better harvesting
	- Added a third Vespene Ridge to Cettocci and Osina mains
	- Added a Vespene Ridge to Cettocci and Osina high-rise expansions
	- Added an additional Mineral Field to highground and lowground side bases
	- Added two Vespene Ridges to interior side bases
- **AI:**
	- Temporarily removed Treasury requests from Osina while improvements are made to better support expanding with Treasuries only
	- Made Osina's expansion sequence more explicit
- **Audiovisuals:**
	- Slightly increased delay before second Osina intro line
	- Changed conditions for "x attacks y's base" events to factories, from structures, to prevent doubling up on dialogues when Osina attacks garrisons
	- Fixed incorrect display name in "Osina attacks Cettocci's base" event

## 18 February 2024
- **Initial prerelease!**