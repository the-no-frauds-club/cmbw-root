# Cosmonarchy

Cosmonarchy, a mod for Starcraft version 1.16.1, is the prototype of the definitive RTS experience. It is the most ambitious Starcraft mod to ever see play.

This is the root directory for all of our maps and editor utilities. It is maintained separately from [release](https://gitlab.com/the-no-frauds-club/cosmonarchy-bw-release) and [prerelease](https://gitlab.com/the-no-frauds-club/cosmonarchy-bw-prerelease) builds of the project itself.

Please send any feedback to us through [our discord server](https://discordapp.com/invite/s5SKBmY), [our forum](https://thenofraudsclub.proboards.com), or [Pr0nogo's email](mailto:Pronogo@hotmail.com).