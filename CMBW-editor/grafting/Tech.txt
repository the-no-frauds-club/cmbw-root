# Tech Requirement Scripts

0:                  # [0] Stim Packs - Maverick
  0xFF02            # Current unit is...
  0                 # [0] Maverick
  0xFF01            # Or
  0xFF02            # Current unit is...
  409               # [409] Ackmed
  0xFFFF            # ==End of List==

1:                  # [1] Lobotomy Grenade
  0xFF02            # Current unit is...
  500               # [500] Goska
  0xFFFF            # ==End of List==

2:                  # [2] Controlled Demolition - Terran Structure
  0xFF02            # Current unit is...
  110               # [110] Reservoir
  0xFF01            # Or
  0xFF02            # Current unit is...
  107               # [107] Comsat Station
  0xFF01            # Or
  0xFF02            # Current unit is...
  108               # [108] Nuclear Silo
  0xFF01            # Or
  0xFF02            # Current unit is...
  115               # [115] Control Tower
  0xFF01            # Or
  0xFF02            # Current unit is...
  117               # [117] Covert Ops
  0xFF01            # Or
  0xFF02            # Current unit is...
  119               # [119] Medbay
  0xFF01            # Or
  0xFF02            # Current unit is...
  120               # [120] Machine Shop
  0xFF01            # Or
  0xFF02            # Current unit is...
  189               # [189] Salvo Yard
  0xFF01            # Or
  0xFF02            # Current unit is...
  121               # [121] Munitions Bay
  0xFF01            # Or
  0xFF02            # Current unit is...
  174               # [174] Quarry
  0xFF01            # Or
  0xFF02            # Current unit is...
  147               # [147] Guildhall
  0xFF01            # Or
  0xFF02            # Current unit is...
  192               # [192] Future Station
  0xFF01            # Or
  0xFF02            # Current unit is...
  220               # [220] Robotics Authority
  0xFF01            # Or
  0xFF02            # Current unit is...
  223               # [223] Monument of Sin
  0xFFFF            # ==End of List==

3:                  # [3] Spider Mines - Vulture
  0xFF1B            # Has spidermines
  0xFF02            # Current unit is...
  2                 # [2] Vulture
  0xFF01            # Or
  0xFF02            # Current unit is...
  418               # [418] Cleric (Futurist)
  0xFFFF            # ==End of List==

4:                  # [4] Scanner Sweep - Comsat Station
  0xFF02            # Current unit is...
  107               # [107] Comsat Station
  0xFFFF            # ==End of List==

5:                  # [5] Terran Deployment
  0xFF02            # Current unit is...
  5                 # [5] Phalanx (Tank Mode)
  0xFF01            # Or
  0xFF02            # Current unit is...
  15                # [15] Shaman (Support Mode)
  0xFF01            # Or
  0xFF02            # Current unit is...
  19                # [19] Cuirass (Tank Mode)
  0xFF01            # Or
  0xFF02            # Current unit is...
  27                # [27] Madrigal (Imperioso Mode)
  0xFF01            # Or
  0xFF02            # Current unit is...
  30                # [30] Phalanx (Siege Mode)
  0xFF01            # Or
  0xFF02            # Current unit is...
  256               # [256] Matador (Tank Mode)
  0xFF01            # Or
  0xFF02            # Current unit is...
  371               # [371] Matador (Cinder Mode)
  0xFF01            # Or
  0xFF02            # Current unit is...
  372               # [372] Cuirass (Trench Mode)
  0xFF01            # Or
  0xFF02            # Current unit is...
  379               # [379] Shaman (Visionary Mode)
  0xFF01            # Or
  0xFF02            # Current unit is...
  381               # [381] Madrigal (Furia Mode)
  0xFFFF            # ==End of List==

6:                  # [6] Defensive Matrix - Azazel
  0xFF02            # Current unit is...
  98                # [98] Azazel
  0xFFFF            # ==End of List==

7:                  # [7] Irradiate - Seraph
  0xFF02            # Current unit is...
  9                 # [9] Seraph
  0xFFFF            # ==End of List==

8:                  # [8] Yamato Cannon - Magnetar
  0xFF02            # Current unit is...
  118               # [118] Magnetar
  0xFFFF            # ==End of List==

9:                  # [9] Select Shared - Zerg Structure
  0xFF02            # Current unit is...
  131               # [131] Hachirosk
  0xFF01            # Or
  0xFF02            # Current unit is...
  132               # [132] Caphrolosk
  0xFF01            # Or
  0xFF02            # Current unit is...
  133               # [133] Gathtelosk
  0xFF01            # Or
  0xFF02            # Current unit is...
  216               # [216] Larvosk Cesiant
  0xFF01            # Or
  0xFF02            # Current unit is...
  383               # [383] Omnivale
  0xFFFF            # ==End of List==

10:                 # [10] Personnel Cloaking
  0xFF02            # Current unit is...
  333               # [333] Sarah Kerrigan
  0xFF01            # Or
  0xFF02            # Current unit is...
  334               # [334] Infested Kerrigan
  0xFFFF            # ==End of List==

11:                 # [11] Burrowing - Zerg Ground
  0xFF02            # Current unit is...
  37                # [37] Zethrokor
  0xFF01            # Or
  0xFF02            # Current unit is...
  39                # [39] Ultrokor
  0xFF01            # Or
  0xFF02            # Current unit is...
  41                # [41] Droleth
  0xFF01            # Or
  0xFF02            # Current unit is...
  46                # [46] Isthrathaleth
  0xFF01            # Or
  0xFF02            # Current unit is...
  50                # [50] Cohort
  0xFF01            # Or
  0xFF02            # Current unit is...
  52                # [52] Evigrilisk
  0xFF01            # Or
  0xFF02            # Current unit is...
  38                # [38] Hydralisk
  0xFF01            # Or
  0xFF02            # Current unit is...
  95                # [95] Quazilisk
  0xFF01            # Or
  0xFF02            # Current unit is...
  96                # [96] Keskathalor
  0xFF01            # Or
  0xFF02            # Current unit is...
  97                # [97] Zoryusthaleth
  0xFF01            # Or
  0xFF02            # Current unit is...
  103               # [103] Lakizilisk
  0xFF01            # Or
  0xFF02            # Current unit is...
  112               # [112] Liiralisk
  0xFF01            # Or
  0xFF02            # Current unit is...
  126               # [126] Kagralisk
  0xFF01            # Or
  0xFF02            # Current unit is...
  217               # [217] Konvilisk
  0xFF01            # Or
  0xFF02            # Current unit is...
  128               # [128] Vorvrokor
  0xFF01            # Or
  0xFF02            # Current unit is...
  129               # [129] Bactalisk
  0xFF01            # Or
  0xFF02            # Current unit is...
  180               # [180] Izirokor
  0xFF01            # Or
  0xFF02            # Current unit is...
  191               # [191] Axitrilisk
  0xFF01            # Or
  0xFF02            # Current unit is...
  267               # [267] Tethzorokor
  0xFF01            # Or
  0xFF02            # Current unit is...
  268               # [268] Protathalor
  0xFF01            # Or
  0xFF02            # Current unit is...
  269               # [269] Vilgorokor
  0xFF01            # Or
  0xFF02            # Current unit is...
  270               # [270] Cikralisk
  0xFF01            # Or
  0xFF02            # Current unit is...
  319               # [319] Mortothrokor
  0xFF01            # Or
  0xFF02            # Current unit is...
  320               # [320] Cherbrathalor
  0xFF01            # Or
  0xFF02            # Current unit is...
  321               # [321] Okrimnilisk
  0xFF01            # Or
  0xFF02            # Current unit is...
  377               # [377] Axitrilisk (Weaponless)
  0xFF01            # Or
  0xFF02            # Current unit is...
  403               # [403] Iziralisk
  0xFF01            # Or
  0xFF02            # Current unit is...
  425               # [425] Lakizilisk (Zorthos)
  0xFF01            # Or
  0xFF02            # Current unit is...
  501               # [501] Stroluum
  0xFFFF            # ==End of List==

12:                 # [12] Infestation - Matraleth
  0xFF02            # Current unit is...
  226               # [226] Grand Library
  0xFFFF            # ==End of List==

13:                 # [13] Ensnaring Brood - Matraleth
  0xFF02            # Current unit is...
  45                # [45] Matraleth
  0xFF01            # Or
  0xFF02            # Current unit is...
  334               # [334] Infested Kerrigan
  0xFFFF            # ==End of List==

14:                 # [14] Carapace Swarm - Isthrathaleth
  0xFF02            # Current unit is...
  46                # [46] Isthrathaleth
  0xFF01            # Or
  0xFF02            # Current unit is...
  501               # [501] Stroluum
  0xFF11            # Is not burrowed
  0xFFFF            # ==End of List==

15:                 # [15] Plague - Ogroszaleth
  0xFF02            # Current unit is...
  46                # [46] Isthrathaleth
  0xFF11            # Is not burrowed
  0xFFFF            # ==End of List==

16:                 # [16] Consume - Zoryusthaleth
  0xFF02            # Current unit is...
  334               # [334] Infested Kerrigan
  0xFF11            # Is not burrowed
  0xFFFF            # ==End of List==

17:                 # [17] Land Grab - Wyvern
  0xFF02            # Current unit is...
  91                # [91] Wyvern
  0xFFFF            # ==End of List==

18:                 # [18] Parasite - Matraleth
  0xFF02            # Current unit is...
  45                # [45] Matraleth
  0xFFFF            # ==End of List==

19:                 # [19] Psionic Storm - Cantavis
  0xFF02            # Current unit is...
  67                # [67] Cantavis
  0xFF01            # Or
  0xFF02            # Current unit is...
  334               # [334] Infested Kerrigan
  0xFFFF            # ==End of List==

20:                 # [20] Hallucinate - Cantavis
  0xFF02            # Current unit is...
  67                # [67] Cantavis
  0xFFFF            # ==End of List==

21:                 # [21] Recall - Didact
  0xFF02            # Current unit is...
  71                # [71] Arbiter
  0xFFFF            # ==End of List==

22:                 # [22] Stasis Field - Didact
  0xFF02            # Current unit is...
  71                # [71] Arbiter
  0xFFFF            # ==End of List==

23:                 # [23] Failure Mode - Anticthon
  0xFF02            # Current unit is...
  211               # [211] Anticthon
  0xFFFF            # ==End of List==

24:                 # [24] Breathing Battery - Savant
  0xFF02            # Current unit is...
  187               # [187] Savant
  0xFFFF            # ==End of List==

25:                 # [25] Disruption Field - Lanifect
  0xFF02            # Current unit is...
  60                # [60] Corsair
  0xFFFF            # ==End of List==

26:                 # [26] Cancel New Ability
  0xFF02            # Current unit is...
  91                # [91] Wyvern
  0xFF01            # Or
  0xFF02            # Current unit is...
  63                # [63] Mind Tyrant
  0xFF01            # Or
  0xFF02            # Current unit is...
  218               # [218] Akistrokor
  0xFF01            # Or
  0xFF02            # Current unit is...
  123               # [123] Sovroleth
  0xFFFF            # ==End of List==

27:                 # [27] Mind Control - Mind Tyrant
  0xFF02            # Current unit is...
  63                # [63] Mind Tyrant
  0xFFFF            # ==End of List==

28:                 # [28] Select Control - Zerg Structure
  0xFF02            # Current unit is...
  131               # [131] Hachirosk
  0xFF01            # Or
  0xFF02            # Current unit is...
  132               # [132] Caphrolosk
  0xFF01            # Or
  0xFF02            # Current unit is...
  133               # [133] Gathtelosk
  0xFF01            # Or
  0xFF02            # Current unit is...
  216               # [216] Larvosk Cesiant
  0xFF01            # Or
  0xFF02            # Current unit is...
  383               # [383] Omnivale
  0xFFFF            # ==End of List==

29:                 # [29] Grief of All Gods - Star Sovereign
  0xFF02            # Current unit is...
  21                # [21] Star Sovereign
  0xFFFF            # ==End of List==

30:                 # [30] Aural Assault - Luminary
  0xFF02            # Current unit is...
  150               # [150] Luminary
  0xFFFF            # ==End of List==

31:                 # [31] Maelstrom - Mind Tyrant
  0xFF02            # Current unit is...
  63                # [63] Mind Tyrant
  0xFFFF            # ==End of List==

32:                 # [32] Lazarus Agent - Apostle
  0xFF02            # Current unit is...
  10                # [10] Apostle
  0xFFFF            # ==End of List==

33:                 # [33] Select Swarm - Zerg Structure
  0xFF02            # Current unit is...
  131               # [131] Hachirosk
  0xFF01            # Or
  0xFF02            # Current unit is...
  132               # [132] Caphrolosk
  0xFF01            # Or
  0xFF02            # Current unit is...
  133               # [133] Gathtelosk
  0xFF01            # Or
  0xFF02            # Current unit is...
  216               # [216] Larvosk Cesiant
  0xFF01            # Or
  0xFF02            # Current unit is...
  383               # [383] Omnivale
  0xFFFF            # ==End of List==

34:                 # [34] Healing - Cleric
  0xFF02            # Current unit is...
  34                # [34] Cleric
  0xFF01            # Or
  0xFF02            # Current unit is...
  418               # [418] Cleric (Futurist)
  0xFFFF            # ==End of List==

35:                 # [35] Blissful Embrace - Zerg
  0xFF02            # Current unit is...
  45                # [45] Matraleth
  0xFFFF            # ==End of List==

36:                 # [36] Tempo Change - Madrigal
  0xFF02            # Current unit is...
  27                # [27] Madrigal
  0xFFFF            # ==End of List==

37:                 # [37] Skyfall - Paladin
  0xFF02            # Current unit is...
  186               # [186] Paladin
  0xFFFF            # ==End of List==

38:                 # [38] Select Assault - Zerg Structure
  0xFF02            # Current unit is...
  131               # [131] Hachirosk
  0xFF01            # Or
  0xFF02            # Current unit is...
  132               # [132] Caphrolosk
  0xFF01            # Or
  0xFF02            # Current unit is...
  133               # [133] Gathtelosk
  0xFF01            # Or
  0xFF02            # Current unit is...
  216               # [216] Larvosk Cesiant
  0xFF01            # Or
  0xFF02            # Current unit is...
  383               # [383] Omnivale
  0xFFFF            # ==End of List==

39:                 # [39] Emergency Detonation - Terran Structure
  0xFF02            # Current unit is...
  110               # [110] Reservoir
  0xFF01            # Or
  0xFF02            # Current unit is...
  107               # [107] Comsat Station
  0xFF01            # Or
  0xFF02            # Current unit is...
  108               # [108] Nuclear Silo
  0xFF01            # Or
  0xFF02            # Current unit is...
  115               # [115] Control Tower
  0xFF01            # Or
  0xFF02            # Current unit is...
  117               # [117] Covert Ops
  0xFF01            # Or
  0xFF02            # Current unit is...
  119               # [119] Medbay
  0xFF01            # Or
  0xFF02            # Current unit is...
  120               # [120] Machine Shop
  0xFF01            # Or
  0xFF02            # Current unit is...
  189               # [189] Salvo Yard
  0xFF01            # Or
  0xFF02            # Current unit is...
  121               # [121] Munitions Bay
  0xFF01            # Or
  0xFF02            # Current unit is...
  174               # [174] Quarry
  0xFF01            # Or
  0xFF02            # Current unit is...
  147               # [147] Guildhall
  0xFF01            # Or
  0xFF02            # Current unit is...
  192               # [192] Future Station
  0xFF01            # Or
  0xFF02            # Current unit is...
  175               # [175] Tinkerer's Tower
  0xFF01            # Or
  0xFF02            # Current unit is...
  190               # [190] Biotic Base
  0xFFFF            # ==End of List==

40:                 # [40] Sublime Shepherd - Azazel
  0xFF02            # Current unit is...
  98                # [98] Azazel
  0xFFFF            # ==End of List==

41:                 # [41] Salvage - Talos
  0xFF02            # Current unit is...
  20                # [20] Talos
  0xFFFF            # ==End of List==

42:                 # [42] Through the Looking Glass - Hypnagogue
  0xFF02            # Current unit is...
  287               # [287] Hypnagogue
  0xFFFF            # ==End of List==

51:                 # [51] Skryspawn - Stroluum
  0xFF02            # Current unit is...
  501               # [501] Stroluum
  0xFF11            # Is not burrowed
  0xFFFF            # ==End of List==